<form action="{{ route('piutangPelangganPembayaranInsert') }}" method="post"
      class="m-form form-send">

    {{ csrf_field() }}
    <input type="hidden" name="id_piutang_pelanggan" value="{{ $row->id_piutang_pelanggan }}">
    <input type="hidden" name="ppp_total_piutang" value="{{ $row->ppl_sisa }}">
    <input type="hidden" name="ppp_sisa_pembayaran" value="{{ $row->ppl_sisa }}">

    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xlg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pembayaran Piutang Pelanggan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Pelanggan</label>
                            <div class="col-9 col-form-label">
                                {{ $row->pelanggan->plg_kode.' '.$row->pelanggan->plg_nama }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Faktur Penjualan</label>
                            <div class="col-9 col-form-label">
                                {{ $row->penjualan->pjl_no_faktur }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Faktur Piutang</label>
                            <div class="col-9 col-form-label">
                                {{ $row->ppl_no_faktur }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Tanggal Piutang</label>
                            <div class="col-9 col-form-label">
                                {{ Main::format_date_label($row->ppl_tanggal) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Jatuh Tempo</label>
                            <div class="col-9 col-form-label">
                                {{ Main::format_date_label($row->ppl_jatuh_tempo) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Total Piutang</label>
                            <div class="col-9 col-form-label">
                                {{ Main::format_number($row->ppl_total) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Sisa Hutang</label>
                            <div class="col-9 col-form-label">
                                {{ Main::format_number($row->ppl_sisa) }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Keterangan Piutang</label>
                            <div class="col-9 col-form-label">
                                {{ $row->ppl_keterangan }}
                            </div>
                        </div>
                        <hr />
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label  required">Tanggal Pembayaran</label>
                            <div class="col-9">
                                <div class="input-group date">
                                    <input type="text"
                                           name="ppp_tanggal_bayar"
                                           class="form-control m-input m_datepicker_1_modal"
                                           readonly=""
                                           value="{{ date('d-m-Y') }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label  required">Keterangan Pembayaran</label>
                            <div class="col-9">
                                <textarea class="form-control" name="ppp_keterangan" placeholder="Tuliskan keterangan untuk pembayaran piutang ..."></textarea>
                            </div>
                        </div>
                        <hr />
                        <div class="form-no-padding">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">
                                    <h3>Total</h3>
                                </label>
                                <div class="col-9">
                                    <h3 class="grand-total" style="padding-left: 15px">{{ Main::format_number($row->ppl_sisa) }}</h3>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">
                                    <h3 class=" required">Terbayar</h3>
                                </label>
                                <div class="col-9">
                                    <input type="text" name="ppp_jumlah_bayar" class="form-control input-numeral" style="font-size: 24px" value="0">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">
                                    <h3>Sisa Pembayaran</h3>
                                </label>
                                <div class="col-9">
                                    <h3 class="sisa-pembayaran" style="padding-left: 15px">0</h3>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">
                                    <h3>Kembalian</h3>
                                </label>
                                <div class="col-9">
                                    <h3 class="kembalian" style="padding-left: 15px">0</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="produksi-buttons">
            <button type="submit" class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-check"></i>
                                <span>Simpan Pembayaran Piutang Pelanggan</span>
                            </span>
            </button>
            <button type="button" class="btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air" data-dismiss="modal">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali</span>
                            </span>
            </button>
        </div>

    </div>



</form>