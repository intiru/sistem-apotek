
    @foreach($stok_barang as $key => $r)
    <tr data-id-stok-barang="{{ $r->id_stok_barang }}"
        data-sbr-kode-batch="{{ $r->sbr_kode_batch }}"
        data-sbr-qty="{{ $r->sbr_qty }}"
        data-sbr-harga-beli="{{ $r->sbr_harga_beli }}"
        data-sbr-harga-jual="{{ $r->sbr_harga_jual }}"
    >
        <td>{{ ++$key }}.</td>
        <td>{{ $r->supplier->spl_kode.'/'.$r->supplier->spl_nama }}</td>
        <td>{{ $r->satuan->stn_nama }}</td>
        <td>{{ $r->sbr_kode_batch }}</td>
        <td>{{ Main::format_date_label($r->sbr_expired) }}</td>
        <td>{!! Main::status($r->sbr_konsinyasi_status) !!} </td>
        <td align="right">{{ Main::format_number($r->sbr_qty) }}</td>
        <td align="right">{{ Main::format_number($r->sbr_harga_jual) }}</td>
        <td>
            <div class="btn-group m-btn-group m-btn--pill">
                <button type="button"
                        class="btn-stok-barang-select btn btn-success btn-sm m-btn--pill">
                    <i class="la la-check"></i> Pilih
                </button>
            </div>
        </td>
    </tr>
    @endforeach