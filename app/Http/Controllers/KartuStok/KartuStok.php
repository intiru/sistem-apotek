<?php

namespace app\Http\Controllers\KartuStok;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHistoryPenyesuaianStok;
use app\Models\mHutangSupplier;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class KartuStok extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->menuActive = $cons['kartu_stok'];
        $this->breadcrumb = [
            [
                'label' => $cons['kartu_stok'],
                'route' => route('kartuStokList')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request);
        $keywords = $filter_component['keywords'];
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "tanggal"],
            ["data" => "nama_barang"],
            ["data" => "no_batch"],
//            ["data" => "expired_date"],
            ["data" => "stok_awal"],
            ["data" => "stok_masuk"],
            ["data" => "stok_keluar"],
            ["data" => "sisa_barang"],
            ["data" => "sisa_total_barang"],
            ["data" => "keterangan"],
            ["data" => "user"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
            ),
        ]);

        return view('kartuStok/kartuStok/kartuStokList', $data);
    }

    function data_table(Request $request)
    {
        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        if ($keywords) {
            $total_data = mArusStok
                ::leftJoin('barang', 'barang.id_barang', '=', 'arus_stok.id_barang')
                ->leftJoin('stok_barang', 'stok_barang.id_stok_barang', '=', 'arus_stok.id_stok_barang')
                ->leftJoin('tb_user', 'tb_user.id', '=', 'arus_stok.id_user')
                ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
                ->whereLike('brg_nama', $keywords)
                ->orWhereLike('brg_kode', $keywords)
                ->orWhereLike('sbr_kode_batch', $keywords)
                ->orWhereLike('asb_keterangan', $keywords)
                ->orWhereLike('nama_karyawan', $keywords)
                ->whereBetween('arus_stok.created_at', $where_date)
                ->count();
        } else {
            $total_data = mArusStok
                ::whereBetween('arus_stok.created_at', $where_date)
                ->count();
        }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_arus_stok'; //$columns[$request->input('order.0.column')];
//        $order_type = $request->input('order.0.dir');
        $order_type = 'asc';

        $data_list = mArusStok
            ::select([
                'arus_stok.created_at AS arus_stok_date',
                'arus_stok.*',
                'barang.*',
                'stok_barang.*',
                'tb_user.*',
                'tb_karyawan.*',
            ])
            ->leftJoin('barang', 'barang.id_barang', '=', 'arus_stok.id_barang')
            ->leftJoin('stok_barang', 'stok_barang.id_stok_barang', '=', 'arus_stok.id_stok_barang')
            ->leftJoin('tb_user', 'tb_user.id', '=', 'arus_stok.id_user')
            ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan');

        if ($keywords) {
            $data_list = $data_list
                ->whereLike('brg_nama', $keywords)
                ->orWhereLike('brg_kode', $keywords)
                ->orWhereLike('sbr_kode_batch', $keywords)
                ->orWhereLike('asb_keterangan', $keywords)
                ->orWhereLike('nama_karyawan', $keywords);
        }

        $data_list = $data_list
            ->whereBetween('arus_stok.created_at', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['tanggal'] = Main::format_datetime($row->arus_stok_date).' '.$row->id_arus_stok;
            $nestedData['nama_barang'] = $row->brg_kode . ' ' . $row->brg_nama;
            $nestedData['no_batch'] = $row->sbr_kode_batch;
//            $nestedData['expired_date'] = Main::format_date_label($row->sbr_expired);
            $nestedData['stok_awal'] = $row->asb_stok_awal >= 0 || $row->asb_stok_awal !== NULL ? Main::format_number($row->asb_stok_awal) : '';
            $nestedData['stok_masuk'] = $row->asb_stok_masuk ? Main::format_number($row->asb_stok_masuk) : '';
            $nestedData['stok_keluar'] = $row->asb_stok_keluar ? Main::format_number($row->asb_stok_keluar) : '';
            $nestedData['sisa_barang'] = Main::format_number($row->asb_stok_terakhir);
            $nestedData['sisa_total_barang'] = Main::format_number($row->asb_stok_total_terakhir);
            $nestedData['keterangan'] = $row->asb_keterangan;
            $nestedData['user'] = $row->nama_karyawan;

            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

}
