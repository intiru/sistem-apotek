<?php

namespace app\Http\Controllers\Penjualan;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHutangSupplier;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Penjualan extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['penjualan'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['penjualan'],
                'route' => route('penjualanList')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "no_faktur"],
            ["data" => "tanggal_penjualan"],
            ["data" => "jenis_pembayaran"],
            ["data" => "grand_total"],
            ["data" => "qty_total"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords
            ),
        ]);

        return view('penjualan/penjualan/penjualanList', $data);
    }

    function data_table(Request $request)
    {
        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mPenjualan
            ::whereBetween('pjl_tanggal_penjualan', $where_date);
        if ($keywords) {
            $total_data = $total_data
                ->whereLike('pjl_no_faktur', $keywords)
                ->orWhereLike('pjl_grand_total', $keywords);
        }
        $total_data = $total_data->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_penjualan'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mPenjualan
            ::select([
                'id_penjualan',
                'pjl_no_faktur',
                'pjl_tanggal_penjualan',
                'pjl_jenis_pembayaran',
                'pjl_grand_total',
            ])
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->withCount([
                'penjualan_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(pjd_qty)'));
                }
            ]);

        if ($keywords) {
            $data_list = $data_list
                ->whereLike('pjl_no_faktur', $keywords)
                ->orWhereLike('pjl_grand_total', $keywords);
        }

        $data_list = $data_list
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_penjualan = Main::encrypt($row->id_penjualan);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['no_faktur'] = $row->pjl_no_faktur;
            $nestedData['tanggal_penjualan'] = Main::format_datetime($row->pjl_tanggal_penjualan);
            $nestedData['jenis_pembayaran'] = $row->pjl_jenis_pembayaran;
            $nestedData['grand_total'] = Main::format_number($row->pjl_grand_total);
            $nestedData['qty_total'] = Main::format_number($row->total_qty);
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('penjualanDetailModal', ['id_penjualan' => $id_penjualan]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function create()
    {
        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Penjualan Baru',
                    'route' => ''
                ]
            ]
        );
        $data = Main::data($breadcrumb, $this->menuActive);
        $pelanggan = mPelanggan::orderBy('plg_nama', 'ASC')->get();
        $barang = mBarang
            ::withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->orderBy('brg_kode')
            ->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $pelanggan_first = $pelanggan[0];

        $data = array_merge(
            $data, array(
                'pelanggan' => $pelanggan,
                'pelanggan_first' => $pelanggan_first,
                'barang' => $barang,
                'satuan' => $satuan
            )
        );

        return view('penjualan/penjualan/penjualanCreate', $data);
    }

    function no_faktur(Request $request)
    {
        $kode_pelanggan = $request->input('kode_pelanggan');
        $id_pelanggan = $request->input('id_pelanggan');
        $pjl_no_faktur = Main::fakturPenjualan($kode_pelanggan);


        return [
            $pjl_no_faktur
        ];
    }

    function stok_barang(Request $request)
    {
        $id_barang = $request->input('id_barang');
        $stok_barang = mStokBarang
            ::with([
                'supplier:id_supplier,spl_kode,spl_nama',
                'satuan:id_satuan,stn_nama'
            ])
            ->where('id_barang', $id_barang)
            ->get();
        $data = [
            'stok_barang' => $stok_barang
        ];

        return view('penjualan/penjualan/tableBarangStok', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'id_pelanggan' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'harga_jual' => 'required',
            'harga_jual.*' => 'required',
            'qty' => 'required',
            'qty.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_pelanggan' => 'Pelanggan',
            'id_barang' => 'Barang',
            'harga_jual' => 'Harga',
            'qty' => 'Qty Barang',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_jual.' . $i] = 'Harga ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_pelanggan = $request->input('id_pelanggan');
        $kode_pelanggan = $request->input('plg_kode');

        $pjl_no_faktur = $request->input('no_faktur');
        $pjl_tanggal_penjualan = Main::format_datetime_db($request->input('tanggal'));
        $pjl_jenis_pembayaran = $request->input('pembayaran');

        $id_barang_arr = $request->input('id_barang');
        $id_stok_barang_arr = $request->input('id_stok_barang');
        $pjd_harga_net_arr = $request->input('harga_net');
        $pjd_sub_total_arr = $request->input('sub_total');
        $pjd_harga_beli_arr = $request->input('harga_beli');
        $pjd_harga_jual_arr = $request->input('harga_jual');
        $pjd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pjd_ppn_persen = $this->ppnPersen;
        $pjd_qty_arr = $request->input('qty');

        $pjl_total = $request->input('total');
        $pjl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pjl_potongan = Main::format_number_db($request->input('potongan'));
        $pjl_grand_total = $request->input('grand_total');
        $pjl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pjl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $ppl_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $pjl_keterangan = $request->input('pjl_keterangan');


        /**
         * Check stok barang semuanya, apakah masih tersedia atau tidak
         */

        $stok_barang_habis = FALSE;
        $stok_barang_now = [];
        foreach ($id_barang_arr as $index => $id_barang) {
            $id_stok_barang = $id_stok_barang_arr[$index];
            $qty_now = mStokBarang::where('id_stok_barang', $id_stok_barang)->value('sbr_qty');
            $qty_beli = Main::format_number_db($pjd_qty_arr[$index]);

            if (array_key_exists($id_stok_barang, $stok_barang_now)) {
//                echo '1, '.($stok_barang_now[$id_stok_barang] - $qty_beli). ' || ';
                $stok_barang_now[$id_stok_barang] = $stok_barang_now[$id_stok_barang] - $qty_beli;
            } else {
//                echo '2, '.($qty_now - $qty_beli).' || ';
                $stok_barang_now[$id_stok_barang] = $qty_now - $qty_beli;
            }

            if (($stok_barang_now[$id_stok_barang]) < 0) {
                $stok_barang_habis = TRUE;
                break;
            }
        }

        if ($stok_barang_habis) {
            return response([
                'errors' => [
                    'stok_barang_habis' => $stok_barang_habis,
                    'stok_barang' => [
                        [
                            0 => 'Stok barang <strong>Tidak Mencukupi</strong> Pembelian, harap check kembali jumlah stok yang tersedia'
                        ]
                    ]
                ]
            ], 422);
        }


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data
             */
            $penjualan_data = [
                'id_pelanggan' => $id_pelanggan,
                'id_user' => $id_user,
                'pjl_no_faktur' => $pjl_no_faktur,
                'pjl_tanggal_penjualan' => $pjl_tanggal_penjualan,
                'pjl_jenis_pembayaran' => $pjl_jenis_pembayaran,
                'pjl_total' => $pjl_total,
                'pjl_biaya_tambahan' => $pjl_biaya_tambahan,
                'pjl_potongan' => $pjl_potongan,
                'pjl_grand_total' => $pjl_grand_total,
                'pjl_jumlah_bayar' => $pjl_jumlah_bayar,
                'pjl_sisa_pembayaran' => $pjl_sisa_pembayaran,
                'pjl_keterangan' => $pjl_keterangan,
                'pjl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
            ];

            $response_penjualan = mPenjualan::create($penjualan_data);
            $id_penjualan = $response_penjualan->id_penjualan;

            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke penjualan detail
                 */
                $id_stok_barang = $id_stok_barang_arr[$index];
                $pjd_harga_net = Main::format_number_db($pjd_harga_net_arr[$index]);
                $pjd_sub_total = $pjd_sub_total_arr[$index];
                $pjd_harga_beli = Main::format_number_db($pjd_harga_beli_arr[$index]);
                $pjd_harga_jual = Main::format_number_db($pjd_harga_jual_arr[$index]);

                $pjd_ppn_nominal = Main::format_number_db($pjd_ppn_nominal_arr[$index]);
                $pjd_qty = Main::format_number_db($pjd_qty_arr[$index]);
                $penjualan_detail_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_barang' => $id_barang,
                    'id_stok_barang' => $id_stok_barang,
                    'pjd_qty' => $pjd_qty,
                    'pjd_harga' => $pjd_harga_jual,
                    'pjd_hpp' => $pjd_harga_beli,
                    'pjd_potongan' => 0,
                    'pjd_ppn_persen' => $pjd_ppn_persen,
                    'pjd_ppn_nominal' => $pjd_ppn_nominal,
                    'pjd_harga_net' => $pjd_harga_net,
                    'pjd_sub_total' => $pjd_sub_total,
                ];

                $response_penjualan_detail = mPenjualanDetail::create($penjualan_detail_data);
                $id_penjualan_detail = $response_penjualan_detail->id_penjualan_detail;

                /**
                 * Mengurangi dari stok barang
                 */

                $sbr_qty_now = mStokBarang::where('id_stok_barang', $id_stok_barang)->value('sbr_qty');
                $asb_stok_terakhir = $sbr_qty_now - $pjd_qty;
                $stok_barang_update = [
                    'sbr_qty' => $asb_stok_terakhir
                ];
                mStokBarang::where('id_stok_barang', $id_stok_barang)->update($stok_barang_update);
                $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');


                $arus_stok_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_penjualan_detail' => $id_penjualan_detail,
                    'id_barang' => $id_barang,
                    'id_stok_barang' => $id_stok_barang,
                    'id_user' => $id_user,
                    'asb_stok_awal' => $sbr_qty_now,
                    'asb_stok_masuk' => 0,
                    'asb_stok_keluar' => $pjd_qty,
                    'asb_stok_terakhir' => $asb_stok_terakhir,
                    'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                    'asb_keterangan' => 'Penjualan Barang No Faktur : ' . $pjl_no_faktur,
                    'asb_tipe_proses' => 'update',
                    'asb_nama_proses' => 'penjualan'
                ];

                mArusStok::create($arus_stok_data);
            }

            if ($pjl_sisa_pembayaran > 0) {
                $ppl_no_faktur = Main::fakturPiutangPelanggan($kode_pelanggan);

                $piutang_pelanggan_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_pelanggan' => $id_pelanggan,
                    'ppl_no_faktur' => $ppl_no_faktur,
                    'ppl_tanggal' => $pjl_tanggal_penjualan,
                    'ppl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
                    'ppl_total' => $pjl_sisa_pembayaran,
                    'ppl_sisa' => $pjl_sisa_pembayaran,
                    'ppl_keterangan' => 'Piutang Pelanggan #' . $ppl_no_faktur . ' dengan no faktur ' . $pjl_no_faktur,
                    'ppl_status' => 'belum_lunas'
                ];

                mPiutangPelanggan::create($piutang_pelanggan_data);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function detail_modal($id_penjualan)
    {
        $id_penjualan = Main::decrypt($id_penjualan);

        $penjualan = mPenjualan
            ::with([
                'pelanggan',
                'user',
                'user.karyawan',
                'penjualan_detail',
                'penjualan_detail.barang',
                'penjualan_detail.stok_barang',
                'penjualan_detail.stok_barang.satuan',
            ])
            ->where('id_penjualan', $id_penjualan)
            ->first();

        $data = [
            'penjualan' => $penjualan,
        ];

        return view('penjualan.penjualan.penjualanDetailModal', $data);
    }
}
