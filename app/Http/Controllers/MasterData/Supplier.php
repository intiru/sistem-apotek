<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mSatuan;
use app\Models\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class Supplier extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_supplier'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mSupplier
            ::orderBy('spl_nama', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('masterData/supplier/supplierList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'spl_nama' => 'required',
            'spl_phone' => 'required',
            'spl_email' => 'required',
            'spl_alamat' => 'required',
        ]);

        $data = $request->except('_token');
        mSupplier::create($data);
    }

    function edit_modal($id_supplier)
    {
        $id_supplier = Main::decrypt($id_supplier);
        $edit = mSupplier::where('id_supplier', $id_supplier)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/supplier/supplierEditModal', $data);
    }

    function delete($id_supplier)
    {
        $id_supplier = Main::decrypt($id_supplier);
        mSupplier::where('id_supplier', $id_supplier)->delete();
    }

    function update(Request $request, $id_supplier)
    {
        $id_supplier = Main::decrypt($id_supplier);
        $request->validate([
            'spl_nama' => 'required',
            'spl_phone' => 'required',
            'spl_email' => 'required',
            'spl_alamat' => 'required',
        ]);
        $data = $request->except("_token");
        mSupplier::where(['id_supplier' => $id_supplier])->update($data);
    }
}
